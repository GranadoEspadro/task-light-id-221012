#!/bin/bash

# Install Nginx
echo "----- Install Nginx ------"
sudo apt update
sudo apt install nginx -y

# Install Apache2-Utils
echo "----- Install Apache2-Utils ------"
sudo apt install apache2-utils -y

sudo mkdir -p /var/www/docker/html
sudo chown -R www-data:www-data /var/www/docker/html
sudo chmod -R 755 /var/www/docker

cat > /etc/nginx/sites-available/docker <<EOF
server {
    listen 80;
    listen [::]:80;
    root /var/www/docker/html;
    index index.html index.htm index.nginx-debian.html;
    server_name  docker www.docker;

    client_max_body_size 100M;
    autoindex off;
    location / {
        proxy_pass                          http://localhost:5000;
        proxy_read_timeout                  900;
    }

    location ~ \.php$ {
         include snippets/fastcgi-php.conf;
         fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
         fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
         include fastcgi_params;
    }
}
EOF

sudo ln -s /etc/nginx/sites-available/docker /etc/nginx/sites-enabled/
sudo unlink /etc/nginx/sites-enabled/default
sudo nginx -t
sudo systemctl restart nginx

# Let’s Encrypt
#echo "----- Add Let’s Encrypt ------"
#sudo apt install snapd -y
#sudo snap install --classic certbot
#sudo ln -s /snap/bin/certbot /usr/bin/certbot
#sudo ufw allow 'Nginx Full'
#sudo ufw delete allow 'Nginx HTTP'
#sudo certbot --nginx

# Add Docker repository
echo "----- Add Docker repository ------"
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository -y \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install Docker
echo "------ Install Docker ------"
sudo apt-get update
sudo apt install docker-ce docker-ce-cli containerd.io -y

# Add Docker user as a non-root user
echo "------ Add Docker user as a non-root user ------"
usermod -aG docker vagrant
newgrp docker

# Install Docker-Compose
echo "------ Install Docker-Compose ------"
sudo apt install docker-compose -y

# Create Docker Storage
echo "------ Create Docker Storage ------"
sudo mkdir /docker-registry
cd /docker-registry
sudo mkdir ./data
sudo mkdir /docker-registry/auth

# Create Login and Password
echo "------ Create Login and Password ------"
cd /docker-registry/auth
sudo htpasswd -Bbc registry.password $user_login_docker_registry $user_password_docker_registry


# Create and up docker-compose
echo "------ Create and up docker-compose ------"
cd /docker-registry
cat > docker-compose.yml <<EOF
version: '3'

services:
  registry:
    image: registry:2
    restart: always
    ports:
    - "5000:5000"
    environment:
      REGISTRY_AUTH: htpasswd
      REGISTRY_AUTH_HTPASSWD_REALM: Registry
      REGISTRY_AUTH_HTPASSWD_PATH: /auth/registry.password
      REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY: /data
    volumes:
      - ./auth:/auth
      - ./data:/data
EOF

sudo systemctl restart nginx
cd /docker-registry
sudo docker-compose up -d

echo "------ Add GitLab repository ------"
curl -q -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

echo "----------install-gitlab-runner----------"
sudo apt-get install -y gitlab-runner

echo "----------register-gitlab-runner----------"
sudo gitlab-runner register -n \
  --url $url \
  --registration-token $token \
  --executor docker \
  --description "docker-registry" \
  --docker-image "Install docker registry" \
  --docker-privileged
